<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heating extends Model
{

  protected $fillable = [
    'id',
    'start',
    'end',
    'temperature',
    ];
    //
}
