<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasstation extends Model
{
  protected $primaryKey = 'id'; // or null

    public $incrementing = false;

  protected $fillable = [
    'id',
    'lat',
    'lng',
    'brand',
    ];
    //
}
