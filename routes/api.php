<?php

use Illuminate\Http\Request;
Use App\Gasstation;
Use App\Heating;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/VWSERVER', function() {
  return '{
    "0": {
      "name": "Golf 7",
      "bild": "http://i.auto-bild.de/i/vw_onepager/vw-golf-7-facelift-vorstellung04.jpg",
      "api": "http://46.101.136.10/api/carInfo"
    },
    "1": {
      "name": "T6",
      "bild": "https://www.tagesspiegel.de/images/20150407vr003/11655632/2-format43.jpg",
      "api": "http://33.0.0.0.1"
    },
    "2": {
      "name": "UP!",
      "bild": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/volkswagen-up.jpg?itok=dxjb9102",
      "api": "http://33.0.0.0.1"
    }
  }';
});





Route::get('/carInfo', function() {
  return response()->json([
    'id' => '0',
    'found' => 'true',
    'name' => 'Golf 7',
    'bild' => 'http://i.auto-bild.de/i/vw_onepager/vw-golf-7-facelift-vorstellung04.jpg',
    'lng' => '9.068871',
    'lat' => '54.09346',
    'position' => '{lat: 54.352815, lng: 10.130217}',
    'type' => 'diesel',
    'litres' => '23.7',
    'averageConsumption' => '5.8',
    'hasNavi' => 'true',
    'hasItunesRadio' => 'true',
    'lastmoved' => '3',
    'icon' => 'http://rlsquads.com/public/golf7.jpg'
]);
});

Route::get('/gasstations', function() {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Gasstation::all();
});

Route::get('gasstations/{id}', function($id) {
    return Gasstation::find($id);
});

Route::post('gasstations', function(Request $request) {
    return Gasstation::create($request->all());
});

Route::delete('gasstations/{id}', function($id) {
    Gasstation::find($id)->delete();

    return 204;
});

Route::get('/heatings', function() {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Heating::all();
});

Route::get('heatings/{id}', function($id) {
    return Heating::find($id);
});

Route::post('heatings', function(Request $request) {
    return Heating::create($request->all());
});

Route::delete('heatings/{id}', function($id) {
    Heating::find($id)->delete();

    return 204;
});
