<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGasstationsTable extends Migration {

	public function up()
	{
		Schema::create('gasstations', function(Blueprint $table) {
			$table->string('id')->unique();
			$table->timestamps();
			$table->string('brand', 255);
			$table->decimal('lat',9,6);
			$table->decimal('lng',9,6);
		});
	}

	public function down()
	{
		Schema::drop('gasstations');
	}
}
